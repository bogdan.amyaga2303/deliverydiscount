//State в классовом и в функциональном компонентах

import React from 'react';
import styles from './names.less';
import {starWars, uniqueNamesGenerator} from 'unique-names-generator'

// 

//Классовый компонент

// 

// interface IStarwarsNameClassState{
//     name: string;
//     count: number;
// }

// export class Names extends React.Component<{}, IStarwarsNameClassState> {
//     //При такой записи - перегрузка - нужно обязательно указывать generic ReadOnly
//     state: Readonly<IStarwarsNameClassState> = {name: '123', count: 0}

//     // constructor(props: {}){
//     //     super(props);
//     //     this.state = {name: '123'};
//     // }

//     public render() {
//         return (
//             <div className={styles.container}>
//                 <span className={styles.name}>
//                     {this.randomName}
//                 </span>
//                 <div className={styles.gap}/>
//                 <button className={styles.button} onClick={this.handleClick}>I need a name</button>
//             </div>
//         )
//     }
//     //приватный(частный) метод компонента, где вызывается метод из npm библиотеки с вложенным объектом-параметрами
//     private randomName(): string {
//         return uniqueNamesGenerator({
//             dictionaries:[starWars],
//             length: 1
//         })
//     }

//     // будем использовать стрелочную функцию, так как у нее есть свой собственный контекст this, чтобы работать со state компонента
//     private handleClick = () => {
//         this.setState({
//             name: this.randomName()
//         })
//         this.setState(state => ({
//             count: state.count + 1
//         }))
//     }
// }
// //Когда setState, то реакт не сразу ее исполняет, он его заносит в очередь, потом исполняет и рендерит (т.е. не сразу) . Чтобы этого не было и реакт сразу рендерил то, что идет после setState, то можно callback

// //Батчинг - способность реакта группировать идущие рядом setState и ререндерит их, поэтому можно использовать несколько подряд setState




//

//Функциональный компонент

//

interface IStarwarsNameFunctionState {
    name: string,
    count: number
}


export function StarWarsNameFunction() {
    const randomName = () => uniqueNamesGenerator({
        dictionaries:[starWars],
        length: 1
    })
    // const [name, setName] = React.useState(randomName)
    // const [count, setCount] = React.useState(0)
    // const handleClick = () => {
    //     setName(randomName)
    //     setCount((prevCount: number) => prevCount + 1)
    // }


    //При таком виде записи нужно использовать spread-оператор, для того, чтобы не перезаписывать те поля состояния, а скопировать, которые не трогаем
    const [state, setState] = React.useState<IStarwarsNameFunctionState>({
        name: randomName(),
        count: 0
    })

    const handleClick = () => {
        //Батчинг также работает, как в классовых компонентах - реакт группирует стейта в группу, применил изменения, и заререндерил компонент
        setState((prevState)=> ({
            ...prevState,
            count: prevState.count + 1
        }))
        setState((prevState)=> ({
            ...prevState, name: randomName()
        }))
    }

    return (
        <section className={styles.example}>
            <span>
                <div/>
                <button onClick={handleClick}>{state.name}</button>
            </span>
        </section>
    )
}


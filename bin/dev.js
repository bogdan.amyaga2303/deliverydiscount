const webpack = require('webpack');
const [ webpackClientConfig, webpackServerConfig ] = require('../webpack.config.js');
const nodemon = require('nodemon');
const path = require('path');
const webpackHotMiddleware = require('webpack-hot-middleware');
const webpackDevMiddleware = require('webpack-dev-middleware');
const express = require('express');

const compiler = webpack(webpackServerConfig);
const clientCompiler = webpack(webpackClientConfig);

const hmrServer = express();

hmrServer.use(webpackDevMiddleware(clientCompiler, {
    //из клиентского конфига настроенный путь /static/ будет совпадать с  hmrserver.
    publicPath: webpackClientConfig.output.publicPath,
    //на стороне сервера будет рендериться приложение
    serverSideRender: true,
    //поле, которое говорит, что вебпак не отдавал много информации, что происходит при бандле
    noInfo: true,
    watchOptions: {
        //когда происходит компиляция, то она меняет файл в папке dist. Чтобы он(devMiddleware) никак не пересобирал и не смотрел в папку dist, нужно установить, в какую папку не нужно смотреть и на нее не надо реагировать
        ignore: '/dist/'
    },
    writeToDisk: true,
    //будет показывать только статистики ошибки
    stats: 'errors-only'
}))

hmrServer.use(webpackHotMiddleware(clientCompiler, {
    //путь должен совпадать с путем, указанным в webpack.client.config. Этот путь позволяет отдавать данные с hmr
    path: '/static/__webpack_hmr'
}));

//запуск hmrсервера (на другом порту)
hmrServer.listen(3001,() => {
    console.log('HMRServer successfully started');
});

compiler.run((err) => {
    if (err) {
        console.log('Compilation failed: ', err)
    }

    compiler.watch({}, (err) => {
        if(err) {
            console.log('Compilation failed: ', err)
        }
        console.log('Compilation was successfully')
    })

    nodemon({
        script: path.resolve(__dirname, '../dist/server/server.js'),
        watch: [
            path.resolve(__dirname, '../dist/server'),
            path.resolve(__dirname, '../dist/client')
            ]
        })
});
